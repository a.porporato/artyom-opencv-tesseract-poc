Little demo project to demostrate how simple it is to integrate:
- [OpenCV.js](https://docs.opencv.org/3.4/d5/d10/tutorial_js_root.html) (image and video acquisition and processing),
- [Tesseract.js](https://tesseract.projectnaptha.com/) (Optical Character Recognition), and
- [Artyom.js](https://sdkcarlos.github.io/sites/artyom.html) (Speech-to-Text and Text-to-Speech)

The web app should be capable of reading aloud text from an image (or camera) on demand (based on tutirial found [here](https://docs.opencv.org/3.4/d0/d84/tutorial_js_usage.html), [here](https://github.com/naptha/tesseract.js#--version-2-is-now-available-and-under-development-in-the-master-branch-read-a-story-about-v2-why-i-refactor-tesseractjs-v2--check-the-support1x-branch-for-version-1) and [here](https://sdkcarlos.github.io/sites/artyom.html)).

To run the application in local, you need to:

1. Retrieve a book with a white cover and a black title written in it

   __Note:__ OCR is quite a challenging problem and in this application we are using vanilla Tesseract without any customized option, so keep the text simple and easily readable (black-on-white, uniform text)
2. From the root folder, run the simplest web server at your disposal (
        
        python -m http.server 8080

   sholud do the trick, if you have Python installed)
3. If requested, grant microphone and webcam use permission to the application
4. Position the book so that the selected sentence occupy the largest part of the image
5. Request the apllication to 'Read'

   __Note:__ Artyom.js permits "always-listening" mode only over HTTPS, so if you are using a simple HTTP server (like the one lauched earlier), make sure that the 'Read' commad is the first that the application listens

The selected sentence should be red aloud and written on the browser console (Tesseract sometimes recognise random shadows or details for character, so it is possible that among the words of the sentence, some random character will be shown. For more advanced applications, consider [Keras OCR](https://keras-ocr.readthedocs.io/en/latest/) as OCR engine).


___NOTE:___ I am *not* a frontend/Javascript developer, so if the code seems bad it probably is. The purposes of this demo is not to show off my (unfortunatley lacking) programming skills, but to demonstrate that ~70 lines of code are enough to produce a basic demo, with the right libraries.
