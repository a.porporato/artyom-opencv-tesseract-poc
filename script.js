const worker = Tesseract.createWorker({
  logger: m => console.log(m)
});

const artyom = new Artyom();

artyom.initialize({
	lang:"en-GB",// A lot of languages are supported. Read the docs !
	continuous:false,// recognize 1 command and stop listening !
	listen:true, // Start recognizing
	debug:true, // Show everything in the console
	speed:1 // talk normally
}).then(function(){
	console.log("Ready to work !");
});

/**
 * Add a command that reacts to "Read"
 */
artyom.addCommands([
	{
		indexes:["Read"],
		action:function(){
			(async () => {
				await worker.load();
				await worker.loadLanguage('eng');
				await worker.initialize('eng');
  
				const { data: { text } } = await worker.recognize(document.getElementById('canvasOutput'));
				
				artyom.say(text,{
					onStart:function(){
						console.log("Said: " + text);
					},
					onEnd:function(){
						console.log("Done saying");
					}
				});
				
				await worker.terminate();
			})();
		}
	}
]);

function onOpenCvReady() {
	cv['onRuntimeInitialized']=()=>{
		console.log("Start");
		let video = document.getElementById('videoInput');
		navigator.mediaDevices.getUserMedia({ video: true, audio: false })
			.then(function(stream) {
				video.srcObject = stream;
				video.play();
			})
			.catch(function(err) {
				console.log("An error occurred! " + err);
			});
		// let video = document.getElementById('videoInput');
		let src = new cv.Mat(video.height, video.width, cv.CV_8UC4);
		let cap = new cv.VideoCapture(video);
		
		const FPS = 30;
		function processVideo() {
			try {
				let begin = Date.now();
				// start processing.
				cap.read(src);
				cv.imshow('canvasOutput', src);
				// schedule the next one.
				let delay = 1000/FPS - (Date.now() - begin);
				setTimeout(processVideo, delay);
			} catch (err) {
				console.log(err);
			}
		};

		// schedule the first one video capture.
		setTimeout(processVideo, 0);

		// setTimeout(artyom.simulateInstruction("Read"), 1000); //Simulate a voice command with voice "read".
  };
}
